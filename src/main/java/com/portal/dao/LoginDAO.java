package com.portal.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.portal.entity.Users;

public interface LoginDAO extends CrudRepository<Users, Integer> {

	public Optional<Users> findByUserNameAndPassword(String userId, String password);
	
	public Optional<Users> findByUserName(String userName);
}

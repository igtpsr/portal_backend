package com.portal.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.portal.entity.TimesheetH;

public interface TimesheetDao extends CrudRepository<TimesheetH, Integer> {

	Optional<TimesheetH> findByEmpIdAndFromDateAndToDate(String empId, Date fromDate, Date toDate);

	@Query(value = "SELECT  th.empId, td.projectId, SUM(td.hours),td.clientName,th.fromDate,th.toDate FROM TimesheetH th INNER JOIN TimesheetD td ON th.id = td.timesheetH where td.clientName=:clientName and th.fromDate=:fromDate and th.toDate=:toDate GROUP BY td.hours,th.empId,td.clientName,td.projectId,th.fromDate,th.toDate")
	public List<Object[]> getTimesheetReportByClientName(@Param("clientName") String clientName,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
}

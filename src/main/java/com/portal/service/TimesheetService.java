package com.portal.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.portal.entity.TimesheetH;

public interface TimesheetService {

	public void saveTimesheet(TimesheetH timesheet);

	public Optional<TimesheetH> getTimesheetByEmpID(String empId, Date fromDate, Date toDate);

	public List<Object[]> getTimesheetReportByClientName(String clientName, Date fromDate, Date toDate);
}

package com.portal.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portal.dao.LoginDAO;
import com.portal.entity.Users;
import com.portal.service.LoginService;

@Service
public class LoginImplementation implements LoginService {

	@Autowired
	private LoginDAO loginDao;

	@Override
	public Optional<Users> login(String userId, String password) {
		return loginDao.findByUserNameAndPassword(userId, password);
	}

}

package com.portal.service.impl;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.portal.dao.LoginDAO;
import com.portal.entity.Users;

@Service
public class CustomUserdetilsService implements UserDetailsService {

	@Autowired
	private LoginDAO loginDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		System.out.println("Username :" + username);
		Optional<Users> modifibleUsers = loginDao.findByUserName(username);

		if (modifibleUsers.isPresent()) {
			return new User(username, modifibleUsers.get().getPassword(), new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("Inavlid user credntials");
		}
	}

}

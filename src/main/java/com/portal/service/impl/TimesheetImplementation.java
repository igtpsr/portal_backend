package com.portal.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portal.dao.TimesheetDao;
import com.portal.entity.TimesheetH;
import com.portal.service.TimesheetService;

@Service
public class TimesheetImplementation implements TimesheetService {

	@Autowired
	private TimesheetDao timesheetDao;

	@Override
	public void saveTimesheet(TimesheetH timesheet) {
		timesheetDao.save(timesheet);
	}

	@Override
	public Optional<TimesheetH> getTimesheetByEmpID(String empId, Date fromDate, Date toDate) {
		return timesheetDao.findByEmpIdAndFromDateAndToDate(empId, fromDate, toDate);
	}

	@Override
	public List<Object[]> getTimesheetReportByClientName(String clientName, Date fromDate, Date toDate) {
		return timesheetDao.getTimesheetReportByClientName(clientName, fromDate, toDate);
	}

}

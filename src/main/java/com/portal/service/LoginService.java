package com.portal.service;

import java.util.Optional;

import com.portal.entity.Users;

public interface LoginService {

	public Optional<Users> login(String userId, String password);

}

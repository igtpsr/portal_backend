package com.portal.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portal.entity.TimesheetD;
import com.portal.entity.TimesheetH;
import com.portal.exception.NoTimesheetFoundException;
import com.portal.model.TimesheetDetails;
import com.portal.model.TimesheetHeader;
import com.portal.service.TimesheetService;
import com.portal.util.Helper;

@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class TimesheetController {

	private final Logger log = LoggerFactory.getLogger(TimesheetController.class);

	@Autowired
	TimesheetService timesheetService;

	@PostMapping(value = "/saveTimesheet")
	public Map<String, String> saveTimesheet(@RequestBody TimesheetHeader timesheet) throws ParseException {

		log.info(String.format("Entering into saveTimesheet %s", timesheet.toString()));
		Map<String, String> data = new HashMap<String, String>();
		try {
			List<TimesheetD> listTD = new ArrayList<TimesheetD>();
			for (TimesheetDetails td : timesheet.getTimesheetDetails()) {
				TimesheetD timesheetD = new TimesheetD();
				timesheetD.setClientName(td.getClientName());
				timesheetD.setComments(td.getComments());
				timesheetD.setDate(Helper.getDate(td.getDate(), "dd-MM-yyyy"));
				timesheetD.setProjectId(td.getProjectId());
				timesheetD.setTaskType(td.getTaskType());
				timesheetD.setHours(td.getHours());
				timesheetD.setCreateTs(new Date());
				timesheetD.setModifyUser(td.getModifyUser());
				timesheetD.setModifyTs(new Date());
				listTD.add(timesheetD);
			}

			TimesheetH timesheetH = new TimesheetH();
			timesheetH.setEmpId(timesheet.getEmpId());
			timesheetH.setFromDate(Helper.getDate(timesheet.getFromDate(), "yyyy/MM/dd"));
			timesheetH.setToDate(Helper.getDate(timesheet.getToDate(), "yyyy/MM/dd"));
			timesheetH.setRemarks(timesheet.getRemarks());
			timesheetH.setStatus(timesheet.getStatus());
			timesheetH.setTotalHours(timesheet.getTotalHours());
			timesheetH.setCreateTs(new Date());
			timesheetH.setModifyUser(timesheet.getModifyUser());
			timesheetH.setModifyTs(new Date());

			for (TimesheetD sd : listTD) {
				sd.setTimesheetH(timesheetH);
			}

			timesheetH.setTimesheetD(listTD);
			timesheetService.saveTimesheet(timesheetH);

			data.put("success", "Successfully submitted Timesheet");
		} catch (Exception e) {
			log.error(String.format("Error Occured while saving Timesheet %s", e.getMessage()));
		}
		return data;
	}

	@GetMapping(value = "/getTimesheetByEmpID")
	public Optional<TimesheetH> getTimesheetByEmpID(@RequestParam String empId, @RequestParam String fromDate,
			@RequestParam String toDate) {
		log.info(String.format("Entering into getTimesheetByEmpID %s", empId));
		Optional<TimesheetH> timesheet = null;
		try {
			Date fDate = Helper.getDate(fromDate, "yyyy/MM/dd");
			Date tDate = Helper.getDate(toDate, "yyyy/MM/dd");
			timesheet = timesheetService.getTimesheetByEmpID(empId, fDate, tDate);
			log.info(timesheet.toString());
		} catch (NoTimesheetFoundException e) {
			log.error(String.format("Error Occured while getTimesheetByEmpID %s", e.getMessage()));
		} catch (Exception e) {
			log.error(String.format("Error Occured while getTimesheetByEmpID %s", e.getMessage()));
		}
		return timesheet;
	}

	@GetMapping(value = "/getTimesheetReportByClientName")
	public List<Map<String, Object>> getTimesheetReportByClientName(@RequestParam String clientName,
			@RequestParam String fromDate, @RequestParam String toDate) {
		log.info(String.format("Entering into getTimesheetReportByClientName %s", clientName));
		List<Object[]> timesheet = new ArrayList<>();
		List<Map<String, Object>> listOfTimesheetReports = new ArrayList<>();
		try {
			Date fDate = Helper.getDate(fromDate, "yyyy-MM-dd");
			Date tDate = Helper.getDate(toDate, "yyyy-MM-dd");
			timesheet = timesheetService.getTimesheetReportByClientName(clientName, fDate, tDate);
			if (timesheet.isEmpty()) {
				throw new NoTimesheetFoundException("No Report is found.");
			}
			for (Object[] obj : timesheet) {
				Map<String, Object> reportsAsMap = new LinkedHashMap<>();
				reportsAsMap.put("empId", obj[0]);
				reportsAsMap.put("projectId", obj[1]);
				reportsAsMap.put("totalHours", obj[2]);
				reportsAsMap.put("clientName", obj[3]);
				reportsAsMap.put("fromDate", obj[4]);
				reportsAsMap.put("toDate", obj[5]);
				listOfTimesheetReports.add(reportsAsMap);
			}
		} catch (NoTimesheetFoundException e) {
			log.error(String.format("Error Occured while getTimesheetReportByClientName %s", e.getMessage()));
		} catch (Exception e) {
			log.error(String.format("Error Occured while getTimesheetReportByClientName %s", e.getMessage()));
		}
		return listOfTimesheetReports;

	}
}

package com.portal.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portal.entity.Users;
import com.portal.exception.CommonCustomExcepton;
import com.portal.util.GoogleGuavaCache;
import com.portal.util.TokenGenerator;

@RestController
@RequestMapping(value = "/api")
public class LoginController {


	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private GoogleGuavaCache googleGuavaCachel;

	private final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@GetMapping(value = "/test")
	public Map<String, Object> testing() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("message", "Successfull");
		return map;
	}

	@PostMapping(value = "/login")
	public Map<String, Object> authentication(@RequestBody Users users) {

		logger.info("Login method called...");

		Map<String, Object> map = new LinkedHashMap<>();
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(users.getuName(), users.getPassword()));
		} catch (Exception e) {
			logger.error("Exception Occured While Authenticate the User");
			throw new CommonCustomExcepton("Invalid username/password");
		}

		String token = TokenGenerator.generateToken(users.getuName());
		googleGuavaCachel.saveTheToken(token, users.getuName());

		map.put("token", token);
		return map;

	}

}

package com.portal.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class AdminController {

	private final Logger log = LoggerFactory.getLogger(AdminController.class);

	@GetMapping(value = "/getClientNames")
	public List<String> getClientNames() {
		log.info(String.format("Entering into getClientNames"));
		List<String> listOfClientNames = new ArrayList<String>();
		try {
			listOfClientNames.add("MGF");
			listOfClientNames.add("AE");
			listOfClientNames.add("BR");
		} catch (Exception e) {
			log.error(String.format("Error Occured while getClientNames %s", e.getMessage()));
		}
		return listOfClientNames;
	}
}

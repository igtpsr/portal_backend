package com.portal.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {

	public static Date getDate(String date, String format) throws ParseException {
		return new SimpleDateFormat(format).parse(date);
	}
}

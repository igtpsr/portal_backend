package com.portal.util;

import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class GoogleGuavaCache {

	private static final Integer EXPIRE_MILISECONDS = 100000;

	private LoadingCache<String, String> tokenCache;

	public GoogleGuavaCache() {
		super();
		tokenCache = CacheBuilder.newBuilder().expireAfterWrite(EXPIRE_MILISECONDS, TimeUnit.SECONDS)
				.build(new CacheLoader<String, String>() {
					public String load(String key) {
						System.err.println("Loading Key :" + key);
						return "";
					}
				});
	}

	// This method is used to return the token against Key->Key values is
	public String getToken(String key) {
		try {
			return tokenCache.get(key);
		} catch (Exception e) {
			return "";
		}
	}

	// This method is used to push the token against Key. Rewrite the token if it
	// exists
	public String saveTheToken(String token, String username) {
		tokenCache.put(token, username);
		return token;
	}

	// This method is used to clear the token catched already
	public void clearToken(String key) {
		tokenCache.invalidate(key);
	}

}

package com.portal.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.filter.OncePerRequestFilter;

import com.portal.exception.BasicAuthenticationException;
import com.portal.service.impl.CustomUserdetilsService;
import com.portal.util.GoogleGuavaCache;

@Component
public class SecurityFilter extends OncePerRequestFilter {

	@Autowired
	private CustomUserdetilsService customUserdetilsService;

	@Autowired
	private GoogleGuavaCache cache;

	private final static Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

	@ExceptionHandler(BasicAuthenticationException.class)
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String authorizationHeader = request.getHeader("Authorization");
		String token = null;
		String username = null;

		String userAgent = request.getHeader("User-Agent");

		logger.info("User Agent :" + userAgent);
		logger.info("authorizationHeader :" + authorizationHeader);


		//&& (userAgent.contains("Chrome") || userAgent.contains("Firefox"))
		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			token = authorizationHeader.substring(7);

			username = cache.getToken(token);

			if (username == "") {
				logger.info("Token is not available");
				throw new BasicAuthenticationException("Expired Token Using..");
			} else {
				UserDetails userDetails = customUserdetilsService.loadUserByUsername(username);
				/* Validate the User Credential */
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());

				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}

		filterChain.doFilter(request, response);
	}

}

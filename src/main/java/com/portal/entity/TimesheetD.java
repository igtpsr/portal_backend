package com.portal.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "Timesheet_D", uniqueConstraints = { @UniqueConstraint(columnNames = "TIMESHEETD_ID") })
@DynamicUpdate
public class TimesheetD implements Serializable {
	private static final long serialVersionUID = -6790693372846798580L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TIMESHEETD_ID", unique = true, nullable = false)
	private Integer detailsId;

	@Column(name = "CLIENT_NAME", unique = false, nullable = false, length = 100)
	private String clientName;
	
	@Column(name = "PROJECT_ID", unique = false, nullable = false, length = 100)
	private String projectId;
	
	@Column(name = "TASK_TYPE", unique = false, nullable = false, length = 100)
	private String taskType;
	
	@Basic
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE", unique = false, nullable = false)
	private Date date;
	
	@Column(name = "COMMENTS", unique = false, nullable = false, length = 250)
	private String comments;
	
	@Column(name = "HOURS", unique = false, nullable = false)
	private int hours;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_TS", unique = false)
	private Date createTs;
	
	@Column(name = "MODIFY_USER", unique = false, length = 20)
	private String modifyUser;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFY_TS", unique = false)
	private Date modifyTs;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="TIMESHEETH_ID")
	private TimesheetH timesheetH;

	public Integer getDetailsId() {
		return detailsId;
	}

	public void setDetailsId(Integer detailsId) {
		this.detailsId = detailsId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public TimesheetH getTimesheetH() {
		return timesheetH;
	}

	public void setTimesheetH(TimesheetH timesheetH) {
		this.timesheetH = timesheetH;
	}

	public Date getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Date modifyTs) {
		this.modifyTs = modifyTs;
	}

}

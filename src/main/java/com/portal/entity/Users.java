package com.portal.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;

@Table(name = "USERS")
@Entity
@DynamicInsert
public class Users implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "USERNAME")
	public String userName;

	@Column(name = "PASSWORD")
	public String password;

	@Column(name = "ROLE_ID")
	public String roleId;

	@Column(name = "EMAIL_ID", unique = true)
	public String email;

	public Users() {
	}

	public Users(int id, String email, String uName, String password, String roleId) {
		super();
		this.id = id;
		this.email = email;
		this.userName = uName;
		this.password = password;
		this.roleId = roleId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getuName() {
		return userName;
	}

	public void setuName(String uName) {
		this.userName = uName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}

package com.portal.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Timesheet_H", uniqueConstraints = { @UniqueConstraint(columnNames = "TIMESHEETH_ID") })
@DynamicUpdate
public class TimesheetH implements Serializable {

	private static final long serialVersionUID = -1798070786993154676L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TIMESHEETH_ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "EMP_ID", unique = false, nullable = false, length = 50)
	private String empId;

	@Basic
	@Temporal(TemporalType.DATE)
	@Column(name = "FROM_DATE", unique = false, nullable = false)
	private Date fromDate;

	@Basic
	@Temporal(TemporalType.DATE)
	@Column(name = "TO_DATE", unique = false, nullable = false)
	private Date toDate;
	
	@Column(name = "STATUS", unique = false, nullable = false, length = 20)
	private String status;
	
	@Column(name = "REMARKS", unique = false, length = 250)
	private String remarks;
	
	@Column(name = "TOTAL_HOURS", unique = false, nullable = false)
	private int totalHours;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_TS", unique = false)
	private Date createTs;
	
	@Column(name = "MODIFY_USER", unique = false, length = 20)
	private String modifyUser;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFY_TS", unique = false)
	private Date modifyTs;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy= "timesheetH")
	private List<TimesheetD> timesheetD;
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Date modifyTs) {
		this.modifyTs = modifyTs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<TimesheetD> getTimesheetD() {
		return timesheetD;
	}

	public void setTimesheetD(List<TimesheetD> timesheetD) {
		this.timesheetD = timesheetD;
	}

	public int getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(int totalHours) {
		this.totalHours = totalHours;
	}
	
	
}

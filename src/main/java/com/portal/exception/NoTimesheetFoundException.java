package com.portal.exception;

public class NoTimesheetFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoTimesheetFoundException(String message) {
		super(message);
	}

}

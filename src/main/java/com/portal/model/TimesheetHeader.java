package com.portal.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TimesheetHeader implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String empId;
	private String fromDate;
	private String toDate;
	private String status;
	private String remarks;
	private int totalHours;
	private List<TimesheetDetails> timesheetDetails;
	private String modifyUser;
	private Date createTs;
	private Date modifyTs;

}

package com.portal.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TimesheetDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String clientName;
	private String projectId;
	private String taskType;
	private String date;
	private String comments;
	private int hours;
	private String modifyUser;
	private Date createTs;
	private Date modifyTs;
}

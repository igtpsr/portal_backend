package com.portal.model;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TimesheetReport implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String empId;
	private String clientName;
	private String projectId;
	private int hours;

}
